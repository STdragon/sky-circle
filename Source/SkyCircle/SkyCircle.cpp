// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkyCircle.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkyCircle, "SkyCircle" );
