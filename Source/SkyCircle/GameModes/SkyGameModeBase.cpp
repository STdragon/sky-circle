// Fill out your copyright notice in the Description page of Project Settings.


#include "SkyGameModeBase.h"
#include "SkyCircle/Pawns/PawnPlayerShip.h"
#include "SkyCircle/PlayerControllers/PlayerControllerBase.h"
#include "Kismet/GameplayStatics.h"


void ASkyGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	HandleGameStart();
}

void ASkyGameModeBase::HandleGameStart()
{
	PlayerShip = Cast<APawnPlayerShip>(UGameplayStatics::GetPlayerPawn(this, 0));
	PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));

	GameStart();
	if (PlayerControllerRef)
	{
		PlayerControllerRef->SetPlayerEnabledState(false);

		FTimerHandle PlayerEnableHandle;
		FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(PlayerControllerRef,
			&APlayerControllerBase::SetPlayerEnabledState, true);
		GetWorld()->GetTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, StartDelay, false);
	}
}
